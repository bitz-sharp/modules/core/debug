﻿using System;
using System.Diagnostics;
using System.Linq;
using Bitz.Modules.Core.Debug.Remoting;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Services;
using Sbatman.Serialize;

namespace Bitz.Modules.Core.Debug
{
    public class DebugService : Service, IDebugService
    {
        private IRemoteInterface _RemoteInterface;
        public enum DebugMessageIDs
        {
            SERVICESTATUS = 1000
        }

        private static Boolean _CurrentModeDebug;

        public override void Initialize()
        {
            _RemoteInterface = Injector.GetSingleton<IRemoteInterface>();
            _RemoteInterface.Connect("127.0.0.1", 4565);
        }

        protected override void Shutdown()
        {
            _RemoteInterface.Disconnect();
            _RemoteInterface.Dispose();
            _RemoteInterface = null;
        }

        public override String GetServiceDebugStatus()
        {
            return "";
        }

        public static Boolean InDebug()
        {
            DebugTest();
            return _CurrentModeDebug;
        }

        [Conditional("DEBUG")]
        private static void DebugTest()
        {
            _CurrentModeDebug = true;
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            RemoteMessage updateServiceStatus = new RemoteMessage();
            Packet serviceData = new Packet(8634);

            foreach (IService service in ServiceManager.ActiveServices.Where(a => a.DebugMode))
            {
                serviceData.Add($"{service.GetType().Name},{service.GetServiceDebugStatus()}");
            }

            updateServiceStatus.ID = (Int16) DebugMessageIDs.SERVICESTATUS;
            updateServiceStatus.Message = Convert.ToBase64String(serviceData.ToByteArray());
            serviceData.Dispose();

            _RemoteInterface.SendMessage(updateServiceStatus);
            _RemoteInterface.TryUpdate(_LastUpdate+ timeSinceLastUpdate);
        }
    }
}