﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;

namespace Bitz.Modules.Core.Debug.Remoting
{
    public interface IRemoteInterface : IInjectable, IUpdateable, IDisposable
    {
        void SendMessage(RemoteMessage message);
        void Connect(String host, Int32 port);
        void Disconnect();
    }
}