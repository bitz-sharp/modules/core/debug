﻿using System;

namespace Bitz.Modules.Core.Debug.Remoting
{
    public struct RemoteMessage
    {
        public Int16 ID;
        public String Message;
    }
}
