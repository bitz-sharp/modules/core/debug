﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Debug.Logging
{
    public class LoggerService : Service, ILoggerService
    {

        private readonly List<ILogConsumer> _RegisteredLogConsumers = new List<ILogConsumer>();

        private readonly List<LogEvent> _OutstandingEvents = new List<LogEvent>();
        private Int32 _ProcessedLogEventsCount;
        private LogSeverity _ActiveSeverityLevels;

        public LogSeverity ActiveSeverityLevels
        {
            get => _ActiveSeverityLevels;
            set => _ActiveSeverityLevels = value;
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            if (_OutstandingEvents.Count <= 0 || _RegisteredLogConsumers.Count == 0) return;

            List<LogEvent> events = null;
            lock (_OutstandingEvents)
            {
                events = _OutstandingEvents.ToList();
                _ProcessedLogEventsCount += _OutstandingEvents.Count;
                _OutstandingEvents.Clear();
            }

            foreach (ILogConsumer consumer in _RegisteredLogConsumers) consumer.ConsumeLogEvent(events);
        }

        protected override void Shutdown()
        {
            _RegisteredLogConsumers?.Clear();
            _OutstandingEvents?.Clear();
        }

        public void Log(LogSeverity Severity, String message)
        {
            if ((Severity & ActiveSeverityLevels) <= 0) return;
            lock (_OutstandingEvents)
            {
                _OutstandingEvents.Add(new LogEvent
                {
                    EventTime = TimeSpan.FromTicks(Environment.TickCount),
                    Message = message,
                    Severity = Severity
                });
            }
        }

        public override String GetServiceDebugStatus()
        {
            return $"PLEC:{_ProcessedLogEventsCount},AS:{ActiveSeverityLevels.ToString()}";
        }

        /// <inheritdoc />
        public override void Initialize()
        {
            IEnumerable<ILogConsumer> consumers = Injector.GetMultiMap<ILogConsumer>();
            foreach (ILogConsumer logConsumer in consumers)
            {
                _RegisteredLogConsumers.Add(logConsumer);
            }
        }

        public void RegisterLogConsumer(ILogConsumer logConsumer)
        {
            if (!_RegisteredLogConsumers.Contains(logConsumer)) _RegisteredLogConsumers.Add(logConsumer);
        }

        public void UnregisterLogConsumer(ILogConsumer logConsumer)
        {
            _RegisteredLogConsumers.Remove(logConsumer);
        }
    }
}