﻿using System;
using System.Collections.Generic;
using System.IO;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Logic;
#if !WEB
using System.Diagnostics;
#endif

namespace Bitz.Modules.Core.Debug.Logging.Consumers
{
    public class LogConsumerFile : BasicObject, ILogConsumer
    {
        private Boolean _HasInit;
        private StreamWriter _OutStream;
        private String _FileName;

        public LogConsumerFile()
        {

        }

        public LogConsumerFile(String filename)
        {
            _FileName = filename;
        }

        public Boolean HasInit()
        {
            Boolean returnValue = _HasInit;
            return returnValue;
        }

        public override void Dispose()
        {
            _OutStream?.Dispose();
            _OutStream = null;
            base.Dispose();
        }

        public void ConsumeLogEvent(LogEvent logEvent)
        {
            _OutStream?.WriteLine($"{logEvent.EventTime:c},{logEvent.Severity},{logEvent.Message}");
            _OutStream?.Flush();
        }

        public void ConsumeLogEvent(List<LogEvent> logEvents)
        {
            foreach (LogEvent logEvent in logEvents)
            {
                _OutStream?.WriteLine($"{logEvent.EventTime:c},{logEvent.Severity},{logEvent.Message}");
            }
            _OutStream?.Flush();
        }

        /// <inheritdoc />
        public void Initialize()
        {
            try
            {
#if!WEB
                _FileName = _FileName ?? $"log{Process.GetCurrentProcess().Id}.txt";
                _OutStream = File.CreateText(_FileName);
#else
                _FileName = _FileName ?? $"log.txt";
                _OutStream = null;
#endif
            }
            catch (Exception e)
            {

            }
            _HasInit = true;
        }
    }
}