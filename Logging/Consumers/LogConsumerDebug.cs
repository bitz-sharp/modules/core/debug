﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Debug.Logging.Consumers
{
    public class LogConsumerDebug : BasicObject, ILogConsumer
    {
        public Boolean HasInit()
        {
            return true;
        }

        public void ConsumeLogEvent(LogEvent logEvent)
        {
#if !WEB
            switch (logEvent.Severity)
            {
                case LogSeverity.WARNING: Console.BackgroundColor = ConsoleColor.DarkYellow; break;
                case LogSeverity.ERROR: Console.BackgroundColor = ConsoleColor.DarkRed; break;
                case LogSeverity.CRITICAL: Console.BackgroundColor = ConsoleColor.Red; break;
                case LogSeverity.CUSTOM1: Console.BackgroundColor = ConsoleColor.DarkCyan; break;
                case LogSeverity.CUSTOM2: Console.BackgroundColor = ConsoleColor.DarkMagenta; break;
                default: Console.BackgroundColor = ConsoleColor.Black; break;
            }
#endif
            String logString = $"{logEvent.EventTime:dd\\.hh\\:mm\\:ss\\:ff}-{logEvent.Severity}-{logEvent.Message}";
            System.Diagnostics.Debug.Print(logString);
            Console.WriteLine(logString);
#if !WEB
            Console.BackgroundColor = ConsoleColor.Black;
#endif
        }

        public void ConsumeLogEvent(List<LogEvent> logEvents)
        {
            foreach (LogEvent lEvent in logEvents)
            {
                ConsumeLogEvent(lEvent);
            }
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }
    }
}