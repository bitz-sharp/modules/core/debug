﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Debug.Logging.Consumers
{
    public class LogConsummerVoid : BasicObject, ILogConsumer
    {
        public Boolean HasInit()
        {
            return true;
        }

        public void ConsumeLogEvent(LogEvent logEvent)
        {
        }

        public void ConsumeLogEvent(List<LogEvent> logEvent)
        {
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }
    }
}